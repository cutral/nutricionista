---
title: Comidas
date: 27/01/21
---

# día: 1 Miércoles
## desayuno
- hr: 8,30
- cant: 1
- alimento y prepración: Té (con 2 cucharaditas de azúcar), 3 galletitas de salvado y queso porsalut, 1 pelón
- apetito: 3
- observación:

## 1/2 mañana
- hr: 11,30
- cant:1
- alimento y prepración: manzana
- apetito:4
- observación:

## almuerzo
- hr: 13,00
- cant: 1
- alimento y prepración: Bife al sartén (con muy poca aceite, sal y pimienta). Ensalada de: tomate, rúcula, remolacha (con sal y aceite)1 rodaja pan integral
- apetito: 4
- observación:

## merienda
- hr: 18
- cant: 1
- alimento y prepración: yogurth con cereales y fibras
- apetito: 4
- observación:

## 1/2 tarde
- hr: 20,00
- cant: 1 
- alimento y prepración: Sandwich de pan integral (una sola rodaja) con 1 feta de lomito horneado y queso
- apetito: 3
- observación:

## cena
- hr: 23
- cant: 1
- alimento y prepración: huevo duro y 1 fruta
- apetito: 1
- observación: Me caí, dolorida de la rodilla

# día: 2 Jueves
## desayuno
- hr: 8,30
- cant: 1
- alimento y prepración: Té (con 2 cucharaditas de azúcar), 3 galletitas de salvado y queso porsalut
- apetito: 3
- observación:

## 1/2 mañana
- hr: 11,00
- cant: 1
- alimento y prepración: banana y 1 galletita de salvado con 1 feta de jamón cocido
- apetito: 3
- observación:

## almuerzo
- hr: 13,00
- cant: 1
- alimento y prepración: porción de carne de cerdo a la parilla con ensalada de lenteja, zanahoria, tomate, huevo duro y chaucha, 1/2 rodaja de pan integral
- apetito: 4
- observación: 15,30 1 Kiwi

## merienda
- hr: 18,30
- cant: 1 
- alimento y prepración: Té (con 2 cucharaditas de azúcar), 3 galletitas de salvado y queso porsalut
- apetito: 3
- observación:

## 1/2 tarde
- hr: 20,00
- cant: 1
- alimento y prepración: banana
- apetito: 3
- observación:

## cena
- hr: 22,00
- cant: 1
- alimento y prepración: Porción de arroz (con queso cremoso, 1 huevo y pimienta) 1 empanada arabe y 1 banana
- apetito: 3
- observación:

# día: 3 Viernes
## desayuno
- hr: 11
- cant: 1
- alimento y prepración: Té (con 2 cucharadas de azúcar) 1 rodaja de pan integral con 3 fetas de queso
- observación: ayuna por resonancia

## 1/2 mañana
- hr: 12,30
- cant: 1/2
- alimento y prepración: durazno
- apetito: 1
- observación:

## almuerzo
- hr: 13,30
- cant: 1
- alimento y prepración: porción de carne de cerdo a la parilla, ensalada de remolacha, huevo duro, tomate, arvejas 1 choclo. Fruta 1 durazno
- apetito: 3
- observación:

## merienda
- hr: 18
- cant: 1
- alimento y prepración: Vaso de jugo de frutas (manzana, durazno en almibar, banana, pelón y jugo de naranja) 1 rodaja de pan integral con 3 fetas de queso
- apetito: 3
- observación:

## 1/2 tarde
- hr:
- cant: 1
- alimento y prepración: durazno
- apetito: 3
- observación:

## cena
- hr: 22,30
- cant: 2 
- alimento y prepración: porciones de tarta de zapallitos (rehogo cebolla y morrón con un poco de aceite) agrego los zapallitos en cubo y los dejo cocinar, agrego sal, pimienta, aji molido), 1 sola tapa de tarta, relleno, 2 fetas de jamón cocido y queso cremoso. 
- apetito: 3
- observación:

# día: 4 Sábado
- desayuno
- hr: 9,20
- cant: 1 Taza
- alimento y prepración: leche con café y 2 de azúcar,1 rodaja de pan integral con 2 fetas de queso
- apetito: 3
- observación:

## 1/2 mañana
- hr: 11,00
- cant: 1
- alimento y prepración: durazno
- apetito: 2
- observación:

## almuerzo
- hr: 13,30
- cant: 6
- alimento y prepración: Sorrentinos de ricotta, jamón y nuez, con salsa de tomate, cebolla, morrón, zanahoria, sal orégano, ají molido, aceite y carne, queso de rallar, 1/2 rodaja de pan integral, 1 banana
- apetito: 3
- observación:

## merienda
- hr: 18,00
- cant: 1 taza
- alimento y prepración: Té (con 2 cucharadas de azúcar) 1 rodaja de pan integral y queso por salut
- apetito: 3
- observación:

## 1/2 tarde
- hr: 19,30
- cant: 1 
- alimento y prepración: yogurth con cereales sin azúcar y friba
- apetito: 3
- observación: Té (con 2 cucharadas de azúcar) 1 rodaja de pan integral y queso por salut

## cena
- hr: 22,00
- cant: 3
- alimento y prepración: Porciones de pizza (casera) harina con levadura, sal, aceite y agua. con salsa de tomate, cebolla, sal oregano y aji molido, queso cremoso. 1/2 vaso de vino
- apetito: 3
- observación:

# día: 5 Domingo
## desayuno
- hr: 9,00
- cant: 1
- alimento y prepración: Té (con 2 cucharadas de azúcar) 1 rodaja de pan integral y queso por salut
- apetito:
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración: Mates amargos con dos galletitas dulces aptas para díabéticos y 1/2 durazno
- apetito: 3
- observación:

## almuerzo
- hr: 13,00
- cant: 1
- alimento y prepración: Milanesa de carne (huevo, con sal provenzal y pimienta, rebosada con pan rallado), 3 rodajas en anco cocinado al horno, ensalada de remolacha, zanahoria, arvejas, tomate y huevo duro
- apetito:3
- observación:

## merienda
- hr: 18.00
- cant: 3 rodajas de melón
- alimento y prepración:
- apetito:3
- observación:

## 1/2 tarde
- hr: 19,00
- cant:
- alimento y prepración:mates con 3 galletitas de salvado y una feta queso chubut
- apetito: 3
- observación:

## cena
- hr: 21,30
- cant: 3
- alimento y prepración: porciones de pizzas
- apetito: 3
- observación:

# día: 6 Lunes
## desayuno
- hr: 8,00
- cant: 1 taza
- alimento y prepración: leche con café (con 2 de azúcar) 1 tostada de pan integral con queso
- apetito: 3
- observación:

## 1/2 mañana
- hr: 11,00
- cant: 1
- alimento y prepración: fruta
- apetito:
- observación:

## almuerzo
- hr: 12,30
- cant:
- alimento y prepración: 1 empanada arabe, 1/2 milanesa de carne con ensalada de lechuga,tomate, remolacha y huevo
- apetito: 4
- observación:

## merienda
- hr: 18,00
- cant: 2
- alimento y prepración: tostadas (criollitas) con tres fetas de queso y mates
- apetito: 3
- observación:

## 1/2 tarde
- hr:
- cant: 2
- alimento y prepración: galletitas de limón
- apetito:
- observación:

## cena
- hr: 21,30
- cant: 2
- alimento y prepración: sandwiches de queso y lomito horneado con pan integral, fruta
- apetito: 3
- observación:

# día: 7 Martes
## desayuno
- hr: 7,30
- cant: 1 taza
- alimento y prepración: leche con café (con 2 de azúcar)2 tostadas (criollitas) 1 feta de queso y dulce de frutilla light
- apetito: 3
- observación:

## 1/2 mañana
- hr: 11,00
- cant: 1
- alimento y prepración: trozo de queso chubut con 2 galletitas de salvado
- apetito: 3
- observación:

## almuerzo
- hr: 14,00
- cant: 2
- alimento y prepración: porciones de tarta de zapallito con una sola tapa, cebolla, morrón, zapallitos, quinoa, queso, aceite, sal, ají molido, pimienta
- apetito: 3
- observación:

## merienda
- hr: 19,30
- cant: 1
- alimento y prepración: sandwich de queso y lomito horneado con pan integral y mates
- apetito: 3
- observación:

## 1/2 tarde
- hr:
- cant: 1
- alimento y prepración: fruta
- apetito:
- observación:

## cena
- hr: 23,00
- cant: 1 Porción
- alimento y prepración: Asado (chorizo, morcilla, costilla de cerdo y carne de vaca) papa al horno 1, ensalada de lechuga y tomate, 1 vaso de vino 1 porción de torta con dulce de leche y chocolate (casera)
- apetito: 3
- observación:

# día: 1 Miércoles
## desayuno
- hr:
- cant: 1
- alimento y prepración:
- apetito: 3
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## almuerzo
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr:
- cant:
- alimento y prepración:
- apetito: 3
- observación:

# día: 2 Jueves
## desayuno
- hr:
- cant:1
- alimento y prepración:
- apetito: 3 
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## almuerzo
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

# día: 3 Viernes
## desayuno
- hr:
- cant: 1
- alimento y prepración:
- apetito:3
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## almuerzo
- hr:
- cant:
- alimento y prepración:
- apetito: 3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

## día: 4 Sábado
- desayuno
- hr:
- cant: 1
- alimento y prepración:
- apetito: 3
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## almuerzo
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr:
- cant:
- alimento y prepración:
- apetito: 3
- observación:

## día: 5 Domingo
- desayuno
- hr: 9
- cant: 1 taza
- alimento y prepración: leche con cafe y 3 galletitas con dulce
- apetito: 3
- observación:

## 1/2 mañana
- hr: 11,30
- cant: 1
- alimento y prepración: fruta
- apetito: 3
- observación:

## almuerzo
- hr: 13,00
- cant: 9 
- alimento y prepración: Sorrentinos de ricota, nuez y jamón cocido con salsa de tomate
- apetito: 3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr: 10,30
- cant: 1 porción
- alimento y prepración: carne de vaca y cerdo a la parrilla, con esnsalada de achicoria, criolla (tomate, cebolla y pimiento). Humus de garbanzo con galletitas de salvado. 1/2 vaso de vino
- apetito: 3
- observación:

# día: 6 Lunes
- hr:
- cant: 1
- alimento y prepración:
- apetito: 3
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## almuerzo
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

# día: 6 Lunes
## desayuno
- hr:
- cant:1
- alimento y prepración:
- apetito:3
- observación:

## 1/2 mañana
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## almuerzo
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:

## merienda
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## 1/2 tarde
- hr:
- cant:
- alimento y prepración:
- apetito:
- observación:

## cena
- hr:
- cant:
- alimento y prepración:
- apetito:3
- observación:
